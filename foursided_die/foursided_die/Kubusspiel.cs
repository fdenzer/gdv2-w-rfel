using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace de.fhworms.inf1743.cubes {
    public sealed class Kubusspiel : Microsoft.Xna.Framework.Game {
        
        #region Attribute und Konstruktor
        
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        // Grundlegende Shaderfunktionen
        private BasicEffect effect;

        #region Matrizen

        private Matrix weltMatrix = Matrix.Identity;

        private Vector3 cameraPosition = new Vector3(0, 0, -100);
        private Matrix viewMatrix = Matrix.Identity;

        private float fieldOfView = MathHelper.Pi / 4;
        private Matrix projMatrix; // = Matrix.Identity;

        #endregion Matrizen

        // Mauszustand
        private MouseState currentMouseState;
        private MouseState lastMouseState;

        // Geometrieobjekte
        Kubus[] cubes;

        /// <summary>
        /// Constructor
        /// </summary>
        public Kubusspiel() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        
        # endregion fields && ctor

        protected override void Initialize() {

            #region Kamera aufsetzen

            viewMatrix = Matrix.CreateLookAt(
                                 cameraPosition,            // Kameraposition 
                                 Vector3.Zero,              // Sichtrichtung
                                 Vector3.Up);               // Richtung ``nach oben''

            projMatrix = Matrix.CreatePerspectiveFieldOfView(
                fieldOfView,                // Gesichtsfeld
                (float)graphics.GraphicsDevice.PresentationParameters.BackBufferWidth / graphics.GraphicsDevice.PresentationParameters.BackBufferHeight,   // Seitenverhältnis
                1.0f,                       // nahe Clipping-Ebene
                1000.0f                     // ferne Clipping-Ebene
            );

            #endregion Kamera

            #region Geometrieobjekte erzeugen

            // Male zwei Kuben
            cubes = new Kubus[2];
            // Vertices
            effect = new BasicEffect(graphics.GraphicsDevice);
            effect.VertexColorEnabled = true;       // Vertexfarben verwenden
            effect.LightingEnabled = false;         // Beleuchtung deaktivieren
            cubes[0] = new Kubus(Matrix.CreateTranslation(50.0f, 25.0f, 100.0f), graphics, effect);
            cubes[1] = new Kubus(new Matrix(), graphics, effect);

            #endregion Objekt-Ecken

            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // einen einfachen Effekt erzeugen
            
        }

        protected override void UnloadContent() {
        }

        protected override void Update(GameTime gameTime) {
            // Exit 
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            KeyboardState CurrentState = Keyboard.GetState();
            if (CurrentState.IsKeyDown(Keys.F1)) {
                graphics.ToggleFullScreen();
            }

            if (CurrentState.IsKeyDown(Keys.Escape)) {
                this.Exit();
            }

            // Kubi drehen
            currentMouseState = Mouse.GetState();
            if (currentMouseState.LeftButton == ButtonState.Pressed) {
                float fX, fY;
                fX = MathHelper.ToRadians(currentMouseState.X - lastMouseState.X);
                fY = MathHelper.ToRadians(currentMouseState.Y - lastMouseState.Y);

                weltMatrix *= Matrix.CreateRotationY(fX) * Matrix.CreateRotationX(-fY);
            }
            int wheel = currentMouseState.ScrollWheelValue;
            if (wheel != 0 && currentMouseState.RightButton == ButtonState.Pressed) {
                var rotation = MathHelper.ToRadians(wheel - lastMouseState.ScrollWheelValue);
                weltMatrix *= Matrix.CreateRotationZ(rotation * 5.0f);
            }
            if (wheel != 0 && currentMouseState.RightButton == ButtonState.Released) {
                var zoom = MathHelper.ToRadians(wheel - lastMouseState.ScrollWheelValue);
                fieldOfView += zoom / 10;  // This value must be between 0 and 180
                if (fieldOfView > 2.5) fieldOfView = 2.5f;
                if (fieldOfView < 0.3f) fieldOfView = 0.3f;
                projMatrix = Matrix.CreatePerspectiveFieldOfView(
                    fieldOfView,
                    (float)graphics.GraphicsDevice.PresentationParameters.BackBufferWidth / graphics.GraphicsDevice.PresentationParameters.BackBufferHeight,   // Seitenverhältnis
                    1.0f,
                    1000.0f
                );
            }

            effect.World = weltMatrix;
            effect.View = viewMatrix;
            effect.Projection = projMatrix;

            base.Update(gameTime);

            lastMouseState = currentMouseState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.SteelBlue);
            var rasterizerState1 = new RasterizerState();
            rasterizerState1.CullMode = CullMode.None;
            graphics.GraphicsDevice.RasterizerState = rasterizerState1;
            foreach (var cube in cubes) {
                cube.draw();
            }
           
            base.Draw(gameTime);
        }
    }
}