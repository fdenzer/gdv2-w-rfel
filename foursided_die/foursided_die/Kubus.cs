﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace de.fhworms.inf1743.cubes {
    public sealed class Kubus {

        # region attributes and constructor

        # region attributes

        private static Vector3 A = new Vector3(-10, -10, -10);
        private static Vector3 B = new Vector3(-10, 10, -10);
        private static Vector3 C = new Vector3(10, -10, -10);
        private static Vector3 D = new Vector3(10, 10, -10);
        private static Vector3 E = new Vector3(-10, 10, 10);
        private static Vector3 F = new Vector3(10, 10, 10);
        private static Vector3 G = new Vector3(10, -10, 10);
        private static Vector3 H = new Vector3(-10, -10, 10);
        
        private VertexBuffer vertexBuffer;
        
        private VertexDeclaration VertexDescr = new VertexDeclaration(
            new VertexElement[] {
                new VertexElement(0, VertexElementFormat.Vector3, VertexElementUsage.Position, 0),          // Position 
                new VertexElement(12, VertexElementFormat.Color, VertexElementUsage.Color, 0)               // Farbe
            }
        );

        // Ziel:
        // I.S.R.O.T
        // Tatsächlich bisher bedacht:
        // skalieren, drehen und Translation als Matrizen

        //todo: hit bei Mausklick oder sogar picking zum verschieben des gewählten Objekts

        private Matrix scale;
        private Matrix rotation;
        private Matrix translation; //vektor reicht
        private Matrix weltmatrix;
        private GraphicsDeviceManager graphics;
        BasicEffect effect;

        #endregion fields

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_translation">Translation matrix</param>
        /// <param name="_graphics">Graphics device mangager</param>
        public Kubus(Matrix _translation, GraphicsDeviceManager _graphics, BasicEffect _effect) {
            translation = _translation;
            graphics = _graphics;
            effect = _effect;
            vertexBuffer = KubusVertexBuffer();

            // - den Vertexbuffer in die Graphikkarte schreiben 
            graphics.GraphicsDevice.SetVertexBuffer(vertexBuffer);
        }

        #endregion fields && ctor

       public void draw() {
            // verschieben in Cube.cs
            foreach (var pass in effect.CurrentTechnique.Passes) {
                pass.Apply();
                // maximal 2 Kubi rendern
                graphics.GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, 24);
            }
        }
        
        
        /// <summary>
        /// Legt die Koordinaten für einen Kubus fest. Dieser kann um vTrans verschoben sein.
        /// </summary>
        /// <param name="vTrans">3D-Vektor zur Translation</param>
        /// <returns></returns>
        public VertexBuffer KubusVertexBuffer() {
            var vertexBuffer = new VertexBuffer(
                                    graphics.GraphicsDevice,            // Gerät
                                    VertexDescr,                        // Vertextyp
                                    36,                                 // Anzahl Vertices bei 6 Seiten * 2 Dreiecke * 3 Punkte 
                                    BufferUsage.None);                  // Benutzung

            VertexPositionColor[] vertices = {
                new VertexPositionColor(A,Color.Green),
                new VertexPositionColor(B,Color.Green),
                new VertexPositionColor(C,Color.Green),
                new VertexPositionColor(B,Color.Green),
                new VertexPositionColor(C,Color.Green),
                new VertexPositionColor(D,Color.Green),
                
                new VertexPositionColor(A,Color.Yellow),
                new VertexPositionColor(H,Color.Yellow),
                new VertexPositionColor(B,Color.Yellow),
                new VertexPositionColor(H,Color.Yellow),
                new VertexPositionColor(B,Color.Yellow),
                new VertexPositionColor(E,Color.Yellow),
                
                new VertexPositionColor(E,Color.Blue),
                new VertexPositionColor(F,Color.Blue),
                new VertexPositionColor(D,Color.Blue),
                new VertexPositionColor(E,Color.Blue),
                new VertexPositionColor(B,Color.Blue),
                new VertexPositionColor(D,Color.Blue),
                
                new VertexPositionColor(G,Color.Red),
                new VertexPositionColor(C,Color.Red),
                new VertexPositionColor(D,Color.Red),
                new VertexPositionColor(G,Color.Red),
                new VertexPositionColor(D,Color.Red),
                new VertexPositionColor(F,Color.Red),
                
                new VertexPositionColor(E,Color.Beige),
                new VertexPositionColor(G,Color.Beige),
                new VertexPositionColor(F,Color.Beige),
                new VertexPositionColor(E,Color.Beige),
                new VertexPositionColor(G,Color.Beige),
                new VertexPositionColor(H,Color.Beige),
                
                new VertexPositionColor(A,Color.Purple),
                new VertexPositionColor(C,Color.Purple),
                new VertexPositionColor(H,Color.Purple),
                new VertexPositionColor(C,Color.Purple),
                new VertexPositionColor(H,Color.Purple),
                new VertexPositionColor(G,Color.Purple)
            };

            // - Vertex-Daten in den Vertexbuffer schreiben 
            vertexBuffer.SetData<VertexPositionColor>(vertices);

            return vertexBuffer;
        }
    }
}